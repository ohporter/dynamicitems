This module provides items that alter your stats by being in your inventory, rather than requiring you to make changes. Think magic items.
### Migrating to 0.4.2 and system 0.7.3.
1. Because of the change to how preparedata works you must upgrade dynamicitems when you upgrade your world. There are no specific migration steps when going from 0.4.0/0.4.1 (system 0.72) to 0.4.2/system 0.7.3 for dynamicitems. dynamicitems will only work with 0.4.2+.

### MIgrating to 0.4.4 and system 0.8.0
There are no specific migration activities but some of the data has changed and you may need to change your items.
1. The new config flag Add Dex Mod to AC (if set - defaults to false) means that ordinary armor compenidum items will auto calculate the character AC. 
2. New effects Melee Weapon attack bonus, Ranged Weapon Attack Bonus, Melee Spell Bonus, Ranged Spell bonus have been added. The new fields are a string, rather than a numeric add, so it will only show up when the roll is made, not on the character sheet. The field should be +1 or +d4 (the plus/minus is important).
3. ANew effect Abiity Check Bonus (same behaviour) but you will need to update any items that set this.  Also a string field see note 2.
4. All Saves has been removed and replaced with Ability Save Bonus. Also a string field see note 2.
5. New field damage bonus added to all weapon attacks (melee/weapon). Also a string field see note 2.
6. New field skill bonus. Also a string field see note 2.


### Notes: Migrating characters with dynamic items in from 0.3.x to 0.4.(0|1)
In prior versions of dynamicitems changes made by the module were permanent to the character. In the 0.4.x version they are not. This requires a migration step, after you have migrated your world to 0.4, to put the character back in the pre-dynamic items state so that the new module will work correclty. 
0. **BACK UP YOUR 0.4.x WORLD.**
0.5 Enable the module.
1. Start your world with the module enabled.
2. From the "Configuration Settings" page, dynamicitems options, disable the "Enaable mod" checkbox and save settings. (This is NOT the manage modules page)
3. Restart the world (return to setup/reenter or ctl-shft-R). This step is **IMPORTANT** - please make sure you restart the world.
4. From the console (F12 to bring it up) enter Dynamicitems.cleanupData() and wait for the conversion to complete.
    The migration will put the charcter back to the pre-dynamic items state, so that the new version will work correctly.
    Any dynamicitems of type "backpack/loot" will be converted to equipment/trinket items with exactly the same properties.
5. Check the characters to make sure they are back to pre-dynamic items state.
6. Renable the "enable mod" checkbox and exit to setup/reenter or reload and everything should be as expected with values correctly set.

- In v0.4.x changes made by dynamicitems only are active while the world is active and the module is active. 
- Under V0.4.x there is no longer a separate character sheet for dynamicitems, only an additional tab (Effects) with all of the effects data.
- If, after migrating, you decide to stop using dynamic items but want to keep the changed values, export the character to json, restart with the module disabled, then reimport the character from json and all changes will be permanent. Once you do this the only way to undo this step is to hand edit the character.

### Main features:
Dynamic items are ones that makes changes to your stats/modifiers when they are active in your inventory. This module is really intended for player characters and NPCs that have linked tokens.
- Armor and shields now update your armor class when equipped and reverse the change when unequipped. (this happens automatically once the module is turned on and the items are equipped). Armor can have dynamic properties, e.g. ac+2 which can require attunement.
- Rings of protection can increase saves or armor class when active, again changes are reversed when unequiped or unattuned (if that is a word).
- There is a straightforward ui to make changes/create items For more complex changes (e.g. AC based on dex mod, you need to know the attribute/ability specification, e.g. @data.abilities.dex.mod)
- Dynamic effects can be applied to any item, weapons/backpacks/armor/feats, in addition to its normal properties.
- Removing an item from your inventory removes its effects on your character.  A few sample items are included in the module.
- Since feats are also items you can create feats that apply changes when equipped - for example improved critical - just drop it onto you character sheet and the critical threshold updates. Please look at some of the sample items to see what sort of things you can do. There is some subtley in configuring items.
- You can use the module to track character changes, e.g. go up a level and get more HP, increase stats - create a feat for the increase and it will both make the change and provide a log of the changes.
- The whole module can be deactivated via a config flag. You must reload the client/return to setup after changning this flag.  It is a good idea to make inactive/remove from characters any items before you do disable the mod or changes will be frozen. if you were wearing a girdle of giant strength you strength will remain at the new level after disabling the mod.
- V 0.17 allows dynamic item effects to be set using the standard ItemSheet5e by adding a tab to the sheet. The features of the tab versus different item sheet are almost identical. If you enable the ItemSheet5e additional TAB every item you edit will have additional flag data attached to it. This should not be a problem.
- v 0.21 adds drop down lists for languages, damage, conditions rather than having to type them in. Also moved all strings into en.json to make internationalisation easier.
- v 0.22 adds skill proficiencies (in addtion to skill mod which is already supported). Adding, for example, proficiency to a skill increases the proficiency by 1 level (changing proficient into expertise). 
- v 0.35 adds automatic additon of dex bonus up to the maximum dex bonus additon in the armor details. No need to add effects for it any longer. Need to enable the module setting "Add Dex Mod"
- v 0.36 fixes an error where negative dex mod was added even for heavy armor with a max dex mod of 0.
- v 0.36 adds support for attack bonuses when these are added to dnd5e.
- v 0.37 adds japanese language support - thanks to @Brother Sharp for the translations.
- v 0.38 displays premade items compendium according to language setting
- v 0.38 Fixes attunement bug.
- v 0.38 remove auto migration code so assumes world is already 0.4+.
- v 0.40 Migration for 0.4.4 and dnd 0.80. Adds support for the new bonus fields in an actor, melee/ranged weapon and spell attacks, damage bonus and abilityCheck/abilitySave bonuses and skillCheckBonus. These are string fields so you can put in a number or something like +1d4 (the plus is important). These are added to the appropriate roll. So you can now implement things like bless or sneak attack as features to drag and drop on your character. (Both of these are included in the premade items)
- v0.4.1 Compendium fixes

### Installation Instructions

To install a module, follow these instructions:

1. [Download the zip]https://gitlab.com/tposney/dynamicitems/raw/master/dynamicitems.zip file included in the module directory.
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  
4. Since this module includes a compendium of items the first time you run with it installed, foundry will complain and not let you open the compendium. Simply return to setup and enter the world a second time and all should be good.

OR use the Foundry modudle install.
1. Paste the url for the module.json https://gitlab.com/tposney/dynamicitems/raw/master/module.json into the install module option and press install.

**DO NOT SET THE DEFAULT SHEET** to DynamicItemSheet5e or every item you edit/create will be a dynamic item. If you do create an item as a dynamic one by mistake, change the item sheet back to the default.

This is an **alpha** release and whilst I have done a lot of testing it is quite possible that something will go wrong and trash your items and or characters. I **strongly** suggest you try this out in a test world first and make sure you can do what you want. I also strongly suggest you make a backup of you world directory before you start playing.

### Creating a magic item

- Create/open an item and change its item sheet to DynamicItemSheet5e. **DO NOT SET THE DEFAULT SHEET** to DynamicItemSheet5e or every item you edit/create will be a dynamic item.
- Edit the item. The interface is much like the inventory interface and you simply add an effect then set its values.
- The dropdown list shows all of the things you can change. For languages/reistance/immunities the field must be all lowercase and match the system name. See ring of fire resitance/ring of common/ring of telepathy for examples. (the ui for this will be improved)
- The mode field (=/+) indicates whether the value should replace/be added to the existing data.
- The system knows a little bit about character attributes, so you can add common as a language and it will behave as expected. (see the sample items for an example).

The included sample items give you a feel for some of the things you can do. Please note, when updating the module the premade items pack will be **OVERWRITTEN** with the one in the module release. So please don't create a libary of items in the module compendium.
The easiest way to play with the mod is to just drop some of the precreated items onto a character and see what happens.



### Notes
- This mod adds a flag to every character when they have dynamic items. This flag, dynamicitems.baseEffectspreEffects holds the character stats before you added any items.
- Most numeric attributes allow you to refer to other character data, e.g. Draconic Resilliance (included in sample items). 
- Player characters (non-gm) have limited access to the effects of an item, they can see, but cannot change, the effects.
- The more complex the changes you make (and the more items that change the same fields) the more likely is that there will be some interaction that makes no sense. 
- There are now 3 similar flags, active, attuned and cursed. 
-  - If an item is cursed its effects are always applied whether equipped or not. This can be useful for non-cursed things as well .e.g. draconic resillaince. Only the GM can set this flag.
-  - If an item is active it does not have to be attuned to affect the charcter, but must still be equipped. Only the GM can set this flag. This is useful for things like leather armor whose AC depends on dex mod, or magic items that do not require attunement.
-  - If an item is attuned and equipped its effects will apply to the chracter. Items that do not nor currently display an attuned flag will for dyanmic items. Player characters can set this field.
-  Setting the additional tab to ItemSheet5e is controlled via a moudle setting.
-  Support for three "special" flags:
-  - All Checks/Modifiers which applies the sepcified change to all ability modifiers (str, dex, con, int, wis, cha)
-  - Languages all. The mode/value are ignored and the character is updated with all languages. 
-  - All skills, lets you apply a change to all skills.

There are different ways to achieve the same effect. If you look at studded leather armor and masterwork leather armor you will see that studded calculates from scratch and masterwork adds a value to the min ac (base ac).
If you drop an item onto a character and it does not do the update you expected, check both the attuned/equipped (and active/cursed) flags to make sure it should be working. Some of the items in the sample items are not attuned by default.

Order matters for some item effects. If you have two belts of giant strength the order of equipping will determine wich is active - this is not per the rules.
There is a bug that editing items on a token do not update the display correctly, but do update the token. Dragging and dropping items onto an unliked token works as expected. If you have an unlinked token, its stats still refer to the actor, so changes made to the actor, including adding magic items will affect the token.

You can use the system to track all changes to a character from the initial roll up, e.g. HP increase, stat changes and so on. 
Chnages are recalculated when you cause prepareData for the character to be called, meaning that changes to the character (including HP) cause the items/feats to recalculate. While not overly expensive one possible use is to enable the mod, add all the effects, items etc to the character and then disable the mod. The changes will remain in place and when you want to do more changes renable the mod, add the items/effects and disable again. 

### Bugs
- **If you want to make permanent changes to your base stats (anything you can edit on the character sheet) by hand YOUR MUST make inactive (and for armor mark unequipped) before changing the base stats. Otherwise any changes you make to stats that the item changes will be overwritten/ignored.**
- Order matters for some item effects.  See note above. Since there can in theory be circular dependencies (add dex mod to strengh and strength mod to dex) which will apply the effects in an arbitrary order.

### Feedback

If you have any suggestions or feedback, please contact me on discord @tposney

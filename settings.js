"use strict";

var DynamicItemsSettings = (function() {
const MODULE_NAME = 'dynamicitems';

let config = {
  /**
   * The Module name
   */
  name: MODULE_NAME,

  /**
   * The module title
   */
  title: "Item Collection Settings",

  /**
   * Some generic path references that might be useful later in the application's windows
   */
  templates: {
    root: `modules/${MODULE_NAME}/`,
    DynamicItemsTabTemplate: `modules/${MODULE_NAME}/templates/dynamicitemstab.html`,
    DynamicItemsEffectSelectorTemplate: `modules/${MODULE_NAME}/templates/effect-selector.html`
  },
  /*
  itemSheetTemplate: `public/modules/${MODULE_NAME}/templates/itemSheet.html`,
  characterSheetTemplate: `public/modules/${MODULE_NAME}/templates/characterSheet.html`,
  */
  /**
   * For each setting, there is are two corresponding entries in the language file to retrieve the translations for
   * - the setting name
   * - the hint displayed beneath the setting's name in the "Configure Game Settings" dialog.
   *
   * Given your MODULE_NAME is 'my-module' and your setting's name is 'EnableCritsOnly', then you will need to create to language entries:
   * {
   *  "my-module.EnableCritsOnly.Name": "Enable critical hits only",
   *  "my-module.EnableCritsOnly.Hint": "Players will only hit if they crit, and otherwise miss automatically *manic laughter*"
   * }
   *
   * The naming scheme is:
   * {
   *  "[MODULE_NAME].[SETTING_NAME].Name": "[TEXT]",
   *  "[MODULE_NAME].[SETTING_NAME].Hint": "[TEXT]"
   * }
   */
  settings: [
    {
      name: "EnableMod",
      scope: "world",
      default: true,
      type: Boolean,
      onChange: (value) => { window.location.reload()}
    },
    {
      name: "AddToDefaultItemSheet",
      scope: "client",
      default: true,
      type: Boolean
    },
    {
      name: "AddDexMod",
      scope: "world",
      default: true,
      type: Boolean
    },
    {
      name: "ConsumeCharge",
      scope: "world",
      default: true,
      type: Boolean
    },
    {
      name: "hideLangPacks",
      scope: "world",
      default: "true",
      type: Boolean
    },
    {
      name: "EnableDebug",
      scope: "client",
      default: false,
      type: Boolean
    },
  ]
};

return config;
})();
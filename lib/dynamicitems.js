var DynamicItems = function () {

  var oldPrepareDataMethod;

  let templates = {};

  let debugLog = false;
  let acAffectingArmorTypes;

  function debug(...args) {
    if (debugLog) {
      console.log(...args);
    }
  }

  var selectEffects = false;

  function readyActions() {
    // cant initialise these until game is ready/loaded
    /* for (let i = 0; i < validMods.length; i++) {
      validMods[i].name = game.i18n.localize(validMods[i].name);
    }
    */
    validMods.sort((a,b) => game.i18n.localize(a.name) < game.i18n.localize(b.name) ? -1 : 1);
  }

  function hidePacks(app, html, other) {
    if (!game.settings.get("dynamicitems", "hideLangPacks")) return;
    html.find('[data-pack]').each(function(li) {
      const pack = game.packs.find(pack => pack.collection === this.dataset.pack);
      if (pack && pack.metadata.lang && pack.collection.startsWith("dynamicitems") && !pack.metadata.lang.includes(game.i18n.lang)) {
        $(this).remove();
      }
    });
  }

  // setup hooks/register sheets
  function setup() {
    DynamicItemsSettings.settings.forEach(setting => {
      let options = {
        name: game.i18n.localize(DynamicItemsSettings.name+"."+setting.name+'.Name'),
        hint: game.i18n.localize(`${DynamicItemsSettings.name}.${setting.name}.Hint`),
        scope: setting.scope,
        config: true,
        default: setting.default,
        type: setting.type,
        onChange: setting.onChange
      };

      if (debugLog) debug("setting options");
      if (debugLog) debug(options);
      if (setting.choices) options.choices = setting.choices;
      game.settings.register(DynamicItemsSettings.name, setting.name, options);
    });
    if (game.settings.get("dynamicitems", "EnableMod")) {
        debugLog = game.settings.get("dynamicitems", "EnableDebug");

      // Register Effect Sheet if the mod is enabled.
      templates = {
        "dynamicitemseffectselectortemplate": DynamicItemsSettings.templates.DynamicItemsEffectSelectorTemplate,
        "dynamicitemstabtemplate": DynamicItemsSettings.templates.DynamicItemsTabTemplate
      }
      if (debugLog) debug("loading templates");
      if (debugLog) debug(templates);   
      loadTemplates(Object.values(templates));

      Hooks.on("renderItemSheet5e", (app, html, data) => {
        if (game.settings.get("dynamicitems", "AddToDefaultItemSheet")) addDynamicItemsTab(app, html, data);
      });
      Hooks.on("renderItemSheet5eDark", (app, html, data) => {
        if (game.settings.get("dynamicitems", "AddToDefaultItemSheet")) addDynamicItemsTab(app, html, data);
      });
      Hooks.on("renderDarkItemSheet5e", (app, html, data) => {
        if (game.settings.get("dynamicitems", "AddToDefaultItemSheet")) addDynamicItemsTab(app, html, data);
      });
      Hooks.on("renderChatMessage", activateHandler);

      // override actor5e prepareData
      oldPrepareDataMethod = CONFIG.Actor.entityClass.prototype.prepareData;
      CONFIG.Actor.entityClass.prototype.prepareData = DynamicItemsPatching.prepareData;
      acAffectingArmorTypes = Object.keys(CONFIG.DND5E.armorTypes).filter(acType => !["trinket"].includes(acType));
    }
  }

  async function addDynamicItemsTab(app, html, data) {
    let item = app.object;
    if (["spell", "bakcpack"].includes(item.type)) return;
    // if (!game.user.isGM && !(item.data.data.attuned || item.data.data.identified)) return;
    if (!hasProperty(item.data, "flags.dynamicitems.active")) setProperty(item.data, "flags.dynamicitems.active", {"type": "Boolean", "label": "Active", "value": false});
    if (!hasProperty(item.data, "flags.dynamicitems.cursed")) setProperty(item.data, "flags.dynamicitems.cursed", {"type": "Boolean", "label": "Cursed", "value": false});
    if (!hasProperty(item.data, "flags.dynamicitems.effects")) setProperty(item.data, "flags.dynamicitems.effects", {"type": "Object", "label": "Effects", "value": []});
    // if (!hasProperty(item.data, "data.attuned")) setProperty(item.data, "data.attuned", {"type": "Boolean", "label": "Attuned", "value": false})

    // clean up previous versions of fields
    if (hasProperty(item.data, "flags.dynamicitems.derivedEffects")) delete item.data.flags.dynamicitems.derivedEffects;
    if (hasProperty(item.data, "flags.dynamicitems.baseEffects")) delete item.data.flags.dynamicitems.baseEffects;

    let tabSelector = html.find(`.sheet-navigation`),
      settingsContainer = html.find(`.sheet-body`),
      dynamicItemsTabString = `<a class="item" data-tab="dynamicitems">${game.i18n.localize("dynamicitems.Title")}</a>`,
      tab = tabSelector.append($(dynamicItemsTabString));
/*      <nav class="sheet-navigation tabs" data-group="primary">
          <a class="item active" data-tab="description">{{ localize "DND5E.Description" }}</a>
          <a class="item" data-tab="details">{{ localize "DND5E.Details" }}</a>
      </nav>
      */
    let dynamicItemsTabTemplate = await renderTemplate(DynamicItemsSettings.templates.DynamicItemsTabTemplate, {
      flags: item.data.flags,
      isGM: game.user.isGM,
      data: {attuned: getProperty(item.data, "data.attuned")},
      needsAttuned : ["tool", "consumable"].includes(item.type)
    });

    let extraTab = settingsContainer.append(dynamicItemsTabTemplate);

    html.find('.dynamicitems .effect-create').click(ev => {
      let effects = item.data.flags.dynamicitems.effects.value || [];
      let newId = effects.length ? Math.max(...effects.map(i => i.id)) + 1 : 1;
      new ItemEffectSelector(item, {id: newId}).render(true);
      selectEffects = true;
    });
  
    // Update Effect Item
    html.find('.dynamicitems .effect-edit').click(ev => {
      let effectId = Number($(ev.currentTarget).parents(".effect").attr("effect-id"));
      new ItemEffectSelector(item, {id: effectId}).render(true);
      selectEffects = true;
    });

    // Delete Effect
    html.find('.dynamicitems .effect-delete').click(async ev => {
      let li = $(ev.currentTarget).parents(".effect"),
      effectId = Number(li.attr("effect-id"));
      let effects = duplicate(item.data.flags.dynamicitems.effects.value) || [];
      for (let i = 0; i < effects.length; i++) {
        if (effects[i].id === effectId) {
          effects.splice(i,1);
        }
      }
      selectEffects = true;
      li.slideUp(200);
      if (item.isOwned) {
        await item.actor.updateOwnedItem({"_id": item._id, "flags.dynamicitems.effects.value": effects})
      } else item.update({"flags.dynamicitems.effects.value": effects});
    });

    if (selectEffects) {
      item.data.flags["_sheetTab"] = "dynamicitems";
      activateTab(html, "dynamicitems")
      selectEffects = false;
    }

  }

  /**
   * Activate a tab by it's name.
   */
  function activateTab(html, tab) {
    tab = html.find(`[data-tab="${tab}"]`);
    tab.siblings().removeClass('active');
    tab.addClass('active');
    tab.show();
  }

  class DynamicItemsPatching {
    static prepareData() {
      if (!game.settings.get("dynamicitems", "EnableMod")) {
        return oldPrepareDataMethod.bind(this)();
      }
      debugLog = game.settings.get("dynamicitems", "EnableDebug");
      if (debugLog) {
        debug(`************************** In prepare data for ${this.name}`); 
        debug(duplicate(this.data));
      }

      // find all effects for the given actor - get all item attribute/ability changes base and derived
      let effects = this.data.items.reduce((effectList, itemData) => effectList.concat(getItemEffects(itemData, this.data)), []);
      if (debugLog) debug(effects);

      let preEffects = getProperty(this.data, "flags.dynamicitems.baseEffectspreEffects") || "{}";
      if (typeof preEffects === "string") preEffects = JSON.parse(preEffects);
      else {
        console.error(`Dynamic Items | Problem with actor ${this.data.name}. Clearing all preEffects`);
        console.log(preEffects);
        preEffects = {};
      }
      if (debugLog) debug("prepare data fetched preeffects are")
      if (debugLog) debug(duplicate(preEffects))
      doBaseEffects(this.data, effects, preEffects);

      // do the existing prepareData
      oldPrepareDataMethod.bind(this)();
      // get effects again in case they depend on any value calcluated in prepareData (only AC at present)
      effects = this.data.items.reduce((effectList, itemData) => effectList.concat(getItemEffects(itemData, this.data)), []);
      // calculate all the dynamic effects and apply them
      let derivedResults = calcAllChanges(this.data, {}, effects, "derivedEffects");
      Object.keys(derivedResults).forEach((attribute) => setProperty(this.data, attribute, derivedResults[attribute]));

      // one more go at baseeffects for things like ac = x+dex.mod
      doBaseEffects(this.data, effects, preEffects);
      if (Object.keys(preEffects).length > 0) {
        setProperty(this.data, "flags.dynamicitems.baseEffectspreEffects", JSON.stringify(preEffects));
      }
      else if (hasProperty(this.data, "flags.dynamicitems")) {
        delete this.data.flags.dynamicitems.baseEffectspreEffects;
      }
    }
  }

  function doBaseEffects(actorData, effects, preEffects) {
    // calculate all the base changes
    // get existing preEffects data

    let changes = calcAllChanges(actorData, preEffects, effects, "baseEffect");
   // update all the base details
    Object.keys(changes).forEach((attribute) => setProperty(actorData, attribute, changes[attribute]));

    // put back any pre change values that are no longer being changed
    if (preEffects !== {}) Object.keys(preEffects).forEach((change) => {
      if (!changes.hasOwnProperty(change)) {// have a preeffect but no change - put back the preEffect
        if (debugLog) {
          debug("putting back preEffect"); 
          debug(change);
        }
        changes[change] = preEffects[change];
        setProperty(actorData, change, preEffects[change]);
        delete preEffects[change];
      }
    });
    if (debugLog) {
      debug("After base Effects "); 
      debug(duplicate(changes)); 
      debug(duplicate(preEffects));
    }
    return changes;
  }
  
  function isActive(itemData) {
    // This test is to allow natural armor effects to be active even if no other flag set.
    if (itemData.data.hasOwnProperty("armor.type") && itemData.data.armor.type.value === "natural") return true; 
    if (!itemData.flags.hasOwnProperty("dynamicitems")) return false;

    if (getProperty(itemData, "flags.dynamicitems.cursed.value")) return true;
    if (hasProperty(itemData.data, "equipped") && !itemData.data.equipped) return false; 
    if (getProperty(itemData.data, "attuned") || getProperty(itemData, "flags.dynamicitems.active.value")) return true;
    return false;
  }

  function getItemEffects(itemData, actorData) {
    let effects = [];
    if (itemData.data.hasOwnProperty("armor") && acAffectingArmorTypes.includes(itemData.data.armor.type) && (itemData.data.equipped || itemData.data.armor.type === "natural")) {
      let acValue = itemData.data.armor.value;
      if (!["shield", "bonus"].includes(itemData.data.armor.type) && game.settings.get("dynamicitems", "AddDexMod")) {
        let dexMod = itemData.data.armor.dex === 0 ? 0 : actorData.data.abilities.dex.mod;
        acValue = acValue + Math.min(itemData.data.armor.dex, dexMod);
      }
      if (itemData.data.armor.value !== 0) effects.push(new ItemEffect(0, "Armor", "data.attributes.ac.value", ["shield", "bonus"].includes(itemData.data.armor.type) ? "+" : "=", acValue, "Number", "baseEffect"))
    }
    if (hasProperty(itemData, "flags.dynamicitems.effects") && isActive(itemData)) effects = effects.concat(itemData.flags.dynamicitems.effects.value);
    // see if there are any special effects to process
    return  effects.reduce((effectList, effect) => effectList.concat(expandSpecial(effect)), [])
  }

  function expandSpecial(effect) {
    let checkList = [];
    switch(effect.effect) {
      case "checks.all":
          ["str", "dex", "con", "int", "wis", "cha"].forEach((stat) =>
            checkList.push(new ItemEffect(0, `${stat} check modifier`, `data.abilities.${stat}.mod`, effect.mode, effect.value, "Number", "derivedEffects")));
            return checkList;
        case "flags.dnd5e.saveBonus":
          ["str", "dex", "con", "int", "wis", "cha"].forEach((stat) =>
            checkList.push(new ItemEffect(0, `${stat} check modifier`, `data.abilities.${stat}.save`, effect.mode, effect.value, "Number", "derivedEffects")));
            return checkList;
        case "data.traits.languages.all":
          return [new ItemEffect(0, "All Languages", "data.traits.languages.value", "=", Object.keys(CONFIG.DND5E.languages), "Array", "baseEffect")];
        case "skills.all":
            Object.keys(CONFIG.DND5E.skills).forEach((skillId)  => 
              checkList.push(new ItemEffect(0, "All Skills", `data.skills.${skillId}.mod`, effect.mode, effect.value, "Roll", "derivedEffects")))
            return checkList;
      default:
        return effect;
    }
  }

  function calcAllChanges(actorData, preEffects, effects, effectType) {
    if (effects.length === 0) return {};
    let changes = {};

    ["=", "+"].forEach((pass) => { // do assigns before adds
      debug("In Pass " + pass);
      effects.forEach((effectObject) => {
        let effect = effectObject.effect;
        if ((effectObject.mode || "=") === pass && effectObject.effectType === effectType) {
            debug("pass matched processing effect "); debug(effectObject); debug("effectObject mode " + effectObject.mode);
            let newValue = effectObject.value;
            try {
              if (effectObject.dataType === "Roll") {
                  newValue = new Roll(effectObject.value, actorData).roll().total; // Roll is mainly to pickup references to other actor data
              }
            } catch (err) {
              newValue = 0;
              console.warn(err)
              console.warn(`Dynamic Items invalid effect specified - please check your items for actor ${actorData.name}`);
              console.warn(`invalid effect is ${effectObject.value}`);
            }
            debug(`new value is ${newValue}`)
            if (hasProperty(actorData, effect) || validFlag(effect)) { // only prcoess effects in the actor, or things flagged as ok (i.e flags that might not be set)
              if (!preEffects.hasOwnProperty(effect)) { // if we have not changed this before record the value the first time it is changed
                preEffects[effect] = getProperty(actorData, effect) || "";
              }

              // if we have made a change use that value as the start (that is the changes so far), otherwise the pre-effect value (i.e. before change)
              // We need to use preEffects rather than just the actor value as the actor value might be the changed from previous effects.
              let oldValue = changes.hasOwnProperty(effect) ? changes[effect] : preEffects.hasOwnProperty(effect) ? preEffects[effect] : hasProperty(actorData, effect) ? getProperty(actorData, effect) : "";
              if (typeof oldValue === "undefined") {// can we make sensible defaults
                if (effectObject.dataType === "number" || effectObject.dataType === "Roll") oldValue= 0;
                if (effectObject.dataType === "string") oldValue= "";
              }

              // check for different types of oldValue- arrays etc
              if (Array.isArray(oldValue)) {
                let newArray = (pass === "+") ? duplicate(oldValue) : [];
                if (Array.isArray(newValue)) newArray = newArray.concat(newValue);
                else newArray.push(newValue);
                changes[effect] = newArray;
              } else {
                // if (pass === "=" && oldValue > newValue) { debug("updating new value to " + oldValue); newValue = oldValue; }// pick the largest value
                if (typeof oldValue === "number") changes[effect] = (pass === "+") ? oldValue + Number(newValue) : Number(newValue);
                else if (!isNaN(Number(newValue))) changes[effect] = ((pass === "+") ? Number(oldValue) : 0) + Number(newValue);
                else changes[effect] = (pass === "+") ? oldValue + newValue : newValue;
              }
            } else {console.log("DynamicItems | Actor did not have effect " + effect); console.log(actorData);}
          }
      });
    });
    return changes;
  }

  // need these - rather than checking hasProperty as until the flag is set it is not present in the actor.
  function validFlag(effect) {
    return [
      "flags.dnd5e.initiativeAdv",
      "flags.dnd5e.initiativeAlert",
      "flags.dnd5e.initiativeHalfProf",
      "flags.dnd5e.powerfulBuild",
      "flags.dnd5e.savageAttacks",
      "flags.dnd5e.spellDCBonus",
      "flags.dnd5e.saveBonus", // creae a synthetic bonus by hand adding all effects
      "flags.dnd5e.check_all", // create a synthetic bonus by hand adding all effects
      "flags.dnd5e.weaponCriticalThreshold",
      "data.bonuses.mwak",
      "data.bonuses.rwak",
      "data.bonuses.msak",
      "data.bonuses.rsak",
      "data.bonuses.damage",
      "data.bonuses.abilitySave",
      "data.bonuses.abilityCheck",
      "data.bonuses.skillCheck",
      "data.traits.languages.custom",
      "data.traits.languages.all",
      "data.traits.dr.custom",
      "data.traits.di.custom",
      "data.traits.dv.custom",
      "data.traits.ci.custom",
      "skills.all"
    ].indexOf(effect) !== -1;
  }

  class ItemEffectSelector extends FormApplication {
    static get defaultOptions() {
      const options = super.defaultOptions;
      options.id = "effect-selector";
      options.classes = ["dnd5e"];
      options.title = game.i18n.localize("dynamicitems.ItemEffectSelection");
      options.template =  templates.dynamicitemseffectselectortemplate;
      options.height = 275;
      options.width = 275;
      return options;
    }

    /* -------------------------------------------- */
    activateListeners(html) {
      super.activateListeners(html);

      html.find('.effectType').change((ev) => {
      this.options.selected = ev.target.selectedIndex;
      this.render(true, {selected: ev.target.selectedIndex})
      return true;
      });

    }
    filterEffects (includeSpecial = true) {
      let mods = validMods.filter((e) => e.modSpec != "..." && (!e.special || includeSpecial))
      .reduce((effects, e) => {
        effects[e.modSpec] = game.i18n.localize(e.name)
        return effects}, {});
        debug(mods);
      return mods;
    }

    getData() {
      if (!this.options.effectList);
      this.options.effectList = this.filterEffects()
      // Return data
      let effect = this.object.data.flags.dynamicitems.effects.value.find(i => i.id === this.options.id);
      if (!effect) effect = new ItemEffect(this.options.id);
      if (this.options.selected) {
        let selectedModSpec = Object.keys(this.options.effectList)[this.options.selected - 1];
        let effectData = validMods.filter((line) => line.modSpec === selectedModSpec)[0];
        effect = new ItemEffect(this.options.id, effectData.name, effectData.modSpec, "+", 0, effectData.dataType, effectData.effectType, effectData.special);
      }
      this.options.modeList = {"+": "Add","=": "Set"};
      let data =  {
        effects: this.options.effectList,
        languages: CONFIG.DND5E.languages,
        conditions : CONFIG.DND5E.conditionTypes,
        damages: CONFIG.DND5E.damageTypes,
        toolProfs: CONFIG.DND5E.toolProficiencies,
        isLanguage: effect.dataType === "Language",
        isCondition: effect.dataType === "Condition",
        isToolProf: effect.dataType ==="ToolProf",
        isDamage: effect.dataType === "Damage",
        proficiencies: {0: "Not Proficient", 0.5: "Half Proficiency", 1: "Proficient", 2: "Expertise"},
        isProficiency: effect.dataType === "Proficiency",
        modes: this.options.modeList,
        spec: effect,
        mode: this.options.modeList[effect.mode],
        dataType: effect.dataType
      }
      return data;
    }
    _updateObject(event, formData) {
      let effects = this.object.getFlag("dynamicitems", "effects").value || [];
      let effectDetails = validMods.find(i => i.modSpec === formData.modSpec);
      let mode = formData.mode === "Add" ? "+":"=";
      if (formData.languages) formData.value = languages;
      let effectLabel = validMods.find(e=>e.modSpec == formData.modSpec).name;
      let newEffect = new ItemEffect(this.options.id, effectLabel, formData.modSpec, mode, formData.value, effectDetails.dataType, effectDetails.type, effectDetails.special);
      let found = false;
      for (let i = 0; i < effects.length; i++) {
        if (effects[i].id === this.options.id) {
          effects[i] = newEffect;
          found = true;
          break;
        }
      }
      if (!found) effects.push(newEffect);
      this.object.update({"flags.dynamicitems.effects.value": duplicate(effects)});
    }
  }

  class ItemEffect {
    constructor (id, label = "nothing", effect = "", mode="+", value= 0, dataType = "Number", effectType = "baseEffect", special=false) {
      this.id = id;
      this.label = label;
      this.effect = effect;
      this.mode = mode,
      this.value = value;
      this.dataType = dataType;
      this.effectType = effectType;
      this.special = special;
    }
  }

  const validMods = [
    {name: "dynamicitems.Strength", modSpec: "data.abilities.str.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Dexterity", modSpec: "data.abilities.dex.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Constitution", modSpec: "data.abilities.con.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Wisdom", modSpec: "data.abilities.wis.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Intelligence", modSpec: "data.abilities.int.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Charisma", modSpec: "data.abilities.cha.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.All_Modifiers/Checks", modSpec: "checks.all", type: "derivedEffects", special: true, dataType: "Roll"},
    {name: "dynamicitems.Strength_Modifier", modSpec: "data.abilities.str.mod", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Dexterity_Modifier", modSpec: "data.abilities.dex.mod", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Constitution_Modifier", modSpec: "data.abilities.con.mod", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Wisdom_Modifier", modSpec: "data.abilities.wis.mod", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Intelligence_Modifier", modSpec: "data.abilities.int.mod", type: "derivedEffects", special: false, "tdataTypeype": "Roll"},
    {name: "dynamicitems.Charisma_Modifier", modSpec: "data.abilities.cha.mod", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.All_Saves", modSpec: "flags.dnd5e.saveBonus", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.mwakBonus", modSpec: "data.bonuses.mwak", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.rwakBonus", modSpec: "data.bonuses.rwak", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.msakBonus", modSpec: "data.bonuses.msak", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.rsakBonus", modSpec: "data.bonuses.rsak", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.damageBonus", modSpec: "data.bonuses.damage", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Strength_Save", modSpec: "data.abilities.str.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Dexterity_Save", modSpec: "data.abilities.dex.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Constitution_Save", modSpec: "data.abilities.con.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Wisdom_Save", modSpec: "data.abilities.wis.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Intelligence_Save", modSpec: "data.abilities.int.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Charisma_Save", modSpec: "data.abilities.cha.save", type: "derivedEffects", special: false, dataType: "Roll"},
    {name: "dynamicitems.Armor_Class", modSpec: "data.attributes.ac.value", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Max_HP", modSpec: "data.attributes.hp.max", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Damage_Immunities", modSpec: "data.traits.di.value", type: "baseEffect", special: false, dataType: "Damage"}, 
    {name: "dynamicitems.Damage_Immunities_Custom", modSpec: "data.traits.di.custom", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Damage_Resistance", modSpec: "data.traits.dr.value", type: "baseEffect", special: false, dataType: "Damage"},
    {name: "dynamicitems.Damage_Resistance_Custom", modSpec: "data.traits.dr.custom", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Damage_Vunlerability", modSpec: "data.traits.dv.value", type: "baseEffect", special: false, dataType: "Damage"},
    {name: "dynamicitems.Damge_Vulnerability_Custom", modSpec: "data.traits.dv.custom", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Condition_Immunities", modSpec: "data.traits.ci.value", type: "baseEffect", special: false, dataType: "Condition"},
    {name: "dynamicitems.Condition_Immunities_Custom", modSpec: "data.traits.ci.custom", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Languages", modSpec: "data.traits.languages.value", type: "baseEffect", special: false, dataType: "Language"},
    {name: "dynamicitems.Languages_Custom", modSpec: "data.traits.languages.custom", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.Languages_All", modSpec: "data.traits.languages.all", type: "baseEffect", special: true, dataType: "String"},
    {name: "dynamicitems.Acrobatics", modSpec: "data.skills.acr.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Acrobatics_Proficiency", modSpec: "data.skills.acr.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Animal_Handling", modSpec: "data.skills.ani.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Animal_Handling_Proficiency", modSpec: "data.skills.ani.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Athletics", modSpec: "data.skills.ath.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Arcana", modSpec: "data.skills.arc.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Arcana_Proficiency", modSpec: "data.skills.arc.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Athletics_Proficiency", modSpec: "data.skills.ath.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Deception", modSpec: "data.skills.dec.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Deception_Proficiency", modSpec: "data.skills.dec.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.History", modSpec: "data.skills.his.bonus", dataType: "Roll", type: "baseEffect", special: false },
    {name: "dynamicitems.History_Proficiency", modSpec: "data.skills.his.value", dataType: "Proficiency", type: "baseEffect", special: false },
    {name: "dynamicitems.Insight", modSpec: "data.skills.ins.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Insight_Proficiency", modSpec: "data.skills.ins.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Investigation", modSpec: "data.skills.inv.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.PassiveInvestigation", modSpec: "data.skills.inv.passive", dataType: "Roll", type: "derivedEffects", special: false},

    {name: "dynamicitems.Investigation_Proficiency", modSpec: "data.skills.inv.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Intimidation", modSpec: "data.skills.itm.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Intimidation_Proficiency", modSpec: "data.skills.itm.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Medicine", modSpec: "data.skills.med.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Medicine_Proficiency", modSpec: "data.skills.med.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Nature", modSpec: "data.skills.nat.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Nature_Proficiency", modSpec: "data.skills.nat.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Persuasion", modSpec: "data.skills.per.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Persuasion_Proficiency", modSpec: "data.skills.per.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Perception", modSpec: "data.skills.prc.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.PassivePerception", modSpec: "data.skills.prc.passive", dataType: "Roll", type: "derivedEffects", special: false},
    {name: "dynamicitems.Perception_Proficiency", modSpec: "data.skills.prc.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Performance", modSpec: "data.skills.prf.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Performance_Proficiency", modSpec: "data.skills.prf.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Religion", modSpec: "data.skills.rel.bonus", "typdataTypee": "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Religion_Proficiency", modSpec: "data.skills.rel.value", "typdataTypee": "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Sleight_of_Hand", modSpec: "data.skills.slt.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Sleight_of_Hand_Proficiency", modSpec: "data.skills.slt.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Stealth", modSpec: "data.skills.ste.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Stealth_Proficiency", modSpec: "data.skills.ste.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Survival", modSpec: "data.skills.sur.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Survival_Proficiency", modSpec: "data.skills.sur.value", dataType: "Proficiency", type: "baseEffect", special: false},
    {name: "dynamicitems.Initiative", modSpec: "data.attributes.init.total", dataType: "Roll", type: "derivedEffects", special: false},
    {name: "dynamicitems.Initiative_Bonus", modSpec: "data.attributes.init.bonus", dataType: "Roll", type: "baseEffect", special: false},
    {name: "dynamicitems.Speed_Walking", modSpec: "data.attributes.speed.value", dataType: "String", type: "baseEffect", special: false},
    {name: "dynamicitems.Speed_Special", modSpec: "data.attributes.speed.special", dataType: "String", type: "baseEffect", special: false},
    {name: "dynamicitems.Proficiency", modSpec: "data.attributes.prof", dataType: "Roll", type: "derivedEffects", special: false},
    {name: "dynamicitems.Strength_Attacks", modSpec: "...", type: "derivedEffects", special: true, dataType: "Roll"},
    {name: "dynamicitems.Dexterity_Attacks", modSpec: "...", type: "derivedEffects", special: true, dataType: "Roll"},
    {name: "dynamicitems.Spell_Attacks", modSpec: "...", type: "derivedEffects", special: true, dataType: "Roll"},
    {name: "dynamicitems.All_Skills", modSpec: "skills.all", dataType: "Roll", type: "derivedEffects", special: true},
    {name: "dynamicitems.Initiative_Advantage", modSpec: "flags.dnd5e.initiativeAdv", type: "baseEffect", special: false, dataType: "Boolean"},
    {name: "dynamicitems.Initiative_Alert", modSpec: "flags.dnd5e.initiativeAlert", type: "baseEffect", special: false, dataType: "Boolean"},
    {name: "dynamicitems.InitiativeHalfProf", modSpec: "flags.dnd5e.initiativeHalfProf", type: "baseEffect", special: false, dataType: "Boolean"},
    {name: "dynamicitems.Powerful_Build", modSpec: "flags.dnd5e.powerfulBuild", type: "baseEffect", special: false, dataType: "Boolean"},
    {name: "dynamicitems.Savage_Attacks", modSpec: "flags.dnd5e.savageAttacks", type: "baseEffect", special: false, dataType: "Boolean"},
    {name: "dynamicitems.Spell_DC Bonus", modSpec: "flags.dnd5e.spellDCBonus", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Critial_Hit Threshold", modSpec: "flags.dnd5e.weaponCriticalThreshold", type: "baseEffect", special: false, dataType: "Roll"},
    {name: "dynamicitems.Melee_Attack Bonus", modSpec: "...", possiblemodSpec: "flags.dynamicitems.meleeAttackBonus", type: "baseEffect", special:false, dataType: "Roll"},
    {name: "dynamicitems.abilityCheck", modSpec: "data.bonuses.abilityCheck", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.abilitySave", modSpec: "data.bonuses.abilitySave", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.skillCheck", modSpec: "data.bonuses.skillCheck", type: "baseEffect", special: false, dataType: "String"},
    {name: "dynamicitems.tool_proficiency", modSpec: "data.traits.toolProf.value", type: "baseEffect", special: false, dataType: "ToolProf"}, 
];



function cleanupData() {
  // 0.39 -> 0.4 update code
  if (game.settings.get("dynamicitems", "EnableMod")) { 
    ui.notifications.info("Pleas use the 'Configure Settings' panel and set EnableMod to false and restart the wrold before running this cleanup");
    console.log("Dynamic Items | Please use the 'Configure Settings' panel and set EnableMod to false and restart the wrold before running this cleanup");
    return;
  }
  game.actors.entities.filter(act => act.data.flags.dynamicitems && act.data.flags.dynamicitems.baseEffectspreEffects && act.data.flags.dynamicitems.baseEffectspreEffects !== "{}").forEach(
    async (act)=>{
      base = JSON.parse(act.data.flags.dynamicitems.baseEffectspreEffects); 
      console.log(`Dynamic Items | For Actor ${act.name} reverting `); console.log(base); 
      await act.update(base);
    });
    game.actors.entities.filter(act => act.data.flags.dynamicitems && act.data.flags.dynamicitems.baseEffectspreEffects).forEach(
      async (act) => {
      console.log(`Dynamic Items | For Actor ${act.name} removing preEffects`); 
      await act.update({"flags.dynamicitems.-=baseEffectspreEffects": null})
    });
    game.actors.entities.forEach((act) => {
    let items = act.items;
    items.forEach(async (item) => {
      let itemData = duplicate(item.data);
      if (itemData.flags.dynamicitems && itemData.flags.dynamicitems.baseEffects) {
        itemData.flags.dynamicitems["-=baseEffects"] = null;
      }
      if (itemData.flags.dynamicitems && itemData.flags.dynamicitems.derivedEffects) {
        itemData.flags.dynamicitems["-derivedEffects"] = null;
      }
      if (itemData.flags.dynamicitems && (itemData.type === "backpack" || itemData.type === "loot") && itemData.flags.dynamicitems.effects && itemData.flags.dynamicitems.effects.value.length > 0) {
        console.log(`Dynamic Items | Moving item ${item.name} from backpack to equipment`)
        itemData.type = "equipment";
        itemData.data.armor = {value: 0, type: "trinket", dex: null};
        await act.deleteOwnedItem(itemData.id);
        await act.createOwnedItem(itemData);
        console.log("Dynamic Items | updating owned item to be "); console.log(itemData);
      };
    })
    })
    game.items.entities.forEach((item) => {
    let itemData = duplicate(item.data);
    if (itemData.flags.dynamicitems && itemData.flags.dynamicitems.baseEffects) {
      itemData.flags.dynamicitems["-=baseEffects"] = null;
    }
    if (itemData.flags.dynamicitems && itemData.flags.dynamicitems.derivedEffects) {
      itemData.flags.dynamicitems["-=derivedEffects"] = null;
    }
    if (itemData.flags.dynamicitems && (itemData.type === "backpack" || itemData.type === "loot") && itemData.flags.dynamicitems.effects && itemData.flags.dynamicitems.effects.value.length > 0) {
      console.log(`Dynamic Items | Moving item ${item.name} from backpack to equipment`)
      itemData.type = "equipment";
      itemData.data.armor = {value: 0, type: "trinket", dex: null};
      console.log("Dynamic Items | updating item to be "); console.log(itemData);
    };
    item.update(itemData);
    });
  }

  activateHandler = async (message, html, data) => {
    if (!getProperty(message, "data.flags.dynamicitemsEffects")) return;
    let buttons = html.find(".dynamicitems-toggle");
    for (let i = 0; i < buttons.length; i++) {
      let button = buttons[i];
      const speaker = message.data.speaker;
      const actor = game.actors.get(speaker.actor);
      const itemName = button.children[0].id;
      button.addEventListener("click", async (ev) => {
        console.log(`Dynamic Items | Toggling ${itemName}`);
        ev.stopPropagation();
        await toggleEffectActive(actor, itemName); // have to await so that we geet updated status
        messageString = createEffectString(actor, getProperty(message, "data.flags.detailed"))
        message.update({"_id": message._id, "content": messageString})
      });
    };
  }

  effectNameString = function(actor, item, active) {
    let btnStyling = 'width: 22px; height:22px; font-size:10px;line-height:1px';
    btnstyling = "width: 60px; padding:0px; line-height:1px;";
    if (!active) btnstyling = btnstyling.concat("font-weight: bold; color: green");
    let activeString = active ? `${game.i18n.localize("dynamicitems.disable")}` 
                              : `${game.i18n.localize("dynamicitems.enable")}`;
    let buttonID = `${item.name}`;
    btntxt = `<button type="button" id="${item.name}" style="${btnstyling}">${activeString}</button>`;

              
    return `<div class="dynamicitems-toggle dynamicitems-${active ? "active":"inactive"}-button">${item.name}: ${btntxt}</div>`
    //               : `<div class="dynamicitems-toggle id="${item.name}" dynamicitems-inactive-button">${item.name}: ${btntxt}</div>`;
  }

  // comment to make sure the update happened.

  createEffectString = (actor, detailed) => {
    effectString = "";
    for (let item of actor.items) {
      if (item.data.flags.dynamicitems && item.data.flags.dynamicitems.effects && item.data.flags.dynamicitems.effects.value.length > 0) {
        let active = isActive(item.data);
        effectString = effectString.concat(effectNameString(actor, item, active))
        if (detailed) {
          for (let effect of item.data.flags.dynamicitems.effects.value) {
            effectLine = `<div class="dynamicitems-${active ? 'active' : 'inactive'}-itemeffect">${game.i18n.localize(effect.label)} ${effect.mode === "=" ? "=" : ""} ${effect.value}</div>`;
            effectString = effectString.concat(effectLine);
          }
        }
      }
    }
    if (effectString.length === 0) effectString = "No Effects";
    return effectString;
  }
  showDynamicItemEffects = (...args) => showEffects(...args);

  showTokenEffects = (tokenId, itemName, overRide = undefined) => {
    if (!tokenId) return false;
    let token = canvas.tokens.get(tokenId);
    if (!token) return false;;
    let actor = token.actor;
    if (!actor) return false;
    return showEffects(actor, itemName, overRide);
  }

  function showEffects(actor, detailed = false) {
    if (!actor) return;
    let flavor = `${"Effect List"}`;
    let effectString = createEffectString(actor, detailed);

    ChatMessage.create({
      user: game.user._id,
      speaker: {actor: actor, alias: actor.name},
      content: effectString,
      whisper: [game.user._id], // ChatMessage.getWhisperIDs("GM"),
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      flags: {dynamicitemsEffects: true, detailed: detailed}
    });
  }

  toggleTokenEffect = (tokenId, itemName, overRide = undefined) => {
    if (!tokenId) return false;
    let token = canvas.tokens.get(tokenId);
    if (!token) return false;;
    let actor = token.actor;
    if (!actor) return false;
    return toggleEffectActive(actor, itemName, overRide);
  }

  toggleEffectActive = (actor, itemName, overRide = undefined) => {
    let consumeCharge = game.settings.get("dynamicitems", "ConsumeCharge");
    const item = actor ? actor.items.find(i => i.name === itemName && !["spell"].includes(i.data.type)) : null;
    if ( !item ) {
      ui.notifications.warn(`${game.i18n.localize("dynamicitems.noeffect")} ${itemName}`);
      return false;
    }

    let useUpdate = {};
    let consumeUpdate = {};
    let current;
    if (item.data.type === "feat") {
      if (!hasProperty(item.data, "flags.dynamicitems.cursed.value")) {
        console.log(`Dynamic Items | item ${item.name} has no cursed property can't toggle`)
        return false;
      }
      current = item.data.flags.dynamicitems.cursed.value;
      if (overRide !== undefined) {
        current = !overRide;
      }
      useUpdate = {"_id": item._id, "flags.dynamicitems.cursed.value": !current};
    } else { // equip/unequip
      current = item.data.data.equipped;
      if (overRide !== undefined) {
        current = !overRide;
      }
      useUpdate = {"_id": item._id, "data.equipped": !current};
    }
    if (consumeCharge && !current && item.data.data.uses && item.data.data.uses.max > 0) {
      let oldCharges = item.data.data.uses.value;
      if (oldCharges < 1) {
        ui.notifications.info(`${item.name} ${game.i18n.localize(current ? "dynamicitems.active"  : "dynamicitems.inactive")}`)
        if (!current) { // no charges and inactive cant make it active
          ui.notifications.info(`${item.name} ${game.i18n.localize("dynamicitems.noCharges")}`)
          return false;
        }
      }
      consumeUpdate = {"data.uses.value": oldCharges - 1, "_id": item._id}
    }
    ui.notifications.info(`${item.name} ${game.i18n.localize(!current ? "dynamicitems.active"  : "dynamicitems.inactive")}`)
    let update = mergeObject(useUpdate, consumeUpdate);
    actor.updateEmbeddedEntity("OwnedItem", update);
    return !current;
    if (game.Gametime.doIn && false) {
      game.Gametime.doIn({minutes: 1}, DynamicItems.toggleEffectActive, actor, itemName, false)
    }
  }

  async function convertLootToEquipment(itemName) {
    let item = game.items.entities.find(i=>i.name === itemName);
    if (!item) {
        ui.notifications.error(`Could not find ${itemName}`);
        return;
    }
    if (!item.type==="loot") {
      ui.notifications.error(`${itemName} is not of type loot`);
      return;
    }
    newItem = await CONFIG.Item.entityClass.create({"name": `${itemName} (Converted)`, type: "equipment"});
    await newItem.update({"data.description": item.data.data.description, "data.weight": item.data.data.weight, "data.quantity": item.data.data.quanity, "data.price": item.data.data.price, "data.armor.type": "trinket", "img": item.data.img});
    console.log(`${itemName} converted`);
  }
  
  Hooks.on('init', () => { 
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'dynamicitems',
            lang: 'ja',
            dir: 'translations/ja'
        });
    }
} );

  return {
    setup: setup,
    readyActions: readyActions,
    calcAllChanges: calcAllChanges,
    cleanupData: cleanupData,
    validMods: validMods,
    toggleEffectActive: toggleEffectActive,
    toggleTokenEffect: toggleTokenEffect,
    showEffects: showEffects,
    showTokenEffects: showTokenEffects,
    showDynamicItemEffects: showDynamicItemEffects,
    convertLootToEquipment: convertLootToEquipment,
    hidePacks: hidePacks
  }
}();


